﻿#include "salarydatabase.h"

SalaryDatabase::SalaryDatabase() {
  QSettings * settings = new QSettings("settings.ini", QSettings::IniFormat);
  db = QSqlDatabase::addDatabase("QMYSQL");
  db.setHostName(settings->value("server/ip").toString());
  db.setPort(settings->value("server/port").toInt());
  db.setUserName(settings->value("server/user").toString());
  db.setPassword(settings->value("server/password").toString());
  db.setDatabaseName(settings->value("server/database").toString());
}

SalaryDatabase::~SalaryDatabase() {
}

QSqlDatabase& SalaryDatabase::getDB() {
  return this->db;
}

User * SalaryDatabase::Authorization(const QString & username, const QString & password_hash) const {
  QSqlQuery query(db);
  query.prepare("SELECT * FROM users WHERE username=? and password_hash=? and isDeleted=0 and isConfirmed=1");
  query.addBindValue(username);
  query.addBindValue(password_hash);
  query.exec();

  User * user = nullptr;
  if (query.next()) {
    user = new User(query);
  }

  return user;
}

bool SalaryDatabase::Registration(const QString & username, const QString & password_hash, const QString & fio) const {
  QSqlQuery query(db);
  query.prepare("SELECT id FROM users WHERE username=?");
  query.addBindValue(username);
  query.exec();
  if (query.next()) {
    return false;
  }

  query.prepare("INSERT INTO users(username, password_hash, isDeleted, authority, fio, date_receipt, date_dismissial, date_birth, isConfirmed) VALUES(?, ?, ?, ?, ?, CURDATE(), NULL, NULL, 0)");
  query.addBindValue(username);
  query.addBindValue(password_hash);
  query.addBindValue(0);
  query.addBindValue(1);
  query.addBindValue(fio);
  return query.exec();
}

bool SalaryDatabase::openDB() {
  return db.open();
}

QVector<User> SalaryDatabase::getAllUsers() {
  QSqlQuery query(db);
  query.prepare("SELECT * FROM users");
  query.exec();

  QVector<User> users;
  while (query.next()) {
    users.push_back(User(query));
  }

  return users;
}

AllInfoForWorker * SalaryDatabase::getConcreteUser(int id) {
  AllInfoForWorker * forInfo = nullptr;

  QSqlQuery query(db);
  query.prepare("SELECT * FROM users WHERE id=?");
  query.addBindValue(id);
  query.exec();
  if (query.next()) {
    forInfo = new AllInfoForWorker();
    forInfo->user = User(query);

    query.prepare("SELECT * FROM project INNER JOIN list_users on project.id = list_users.id_project WHERE list_users.id_user=?\
                   AND list_users.date_start<=CURDATE() GROUP BY project.name, list_users.position");
    query.addBindValue(id);
    query.exec();
    while (query.next()) {
      forInfo->projects.push_back(Project(query));
      forInfo->helpInfo[forInfo->projects.back().getID()].position = query.value(9).toString();
      forInfo->helpInfo[forInfo->projects.back().getID()].mark = query.value(11).toInt();
    }
  }

  return forInfo;
}

bool SalaryDatabase::updateUser(const User & user) {
  QSqlQuery query(db);
  query.prepare("UPDATE users SET isDeleted=?, fio=?, date_receipt=?, date_dismissial=?, date_birth=?, passport_code=?, \
   passport_tag=?, passport_number=?, passport_date=? WHERE id=?");
  query.addBindValue(int(user.isDeleted()));
  query.addBindValue(user.getFio());
  query.addBindValue(user.getDateReceipt());
  query.addBindValue(user.getDateDismissial());
  query.addBindValue(user.getDateBirth());
  query.addBindValue(user.getPassportCode());
  query.addBindValue(user.getPassportTag());
  query.addBindValue(user.getPassportNumber());
  query.addBindValue(user.getPassportDate());
  query.addBindValue(user.getID());
  return query.exec();
}

QVector<Project> SalaryDatabase::getAllProjects() {
  QVector<Project> projects;
  QSqlQuery query(db);
  query.prepare("SELECT * FROM project");
  query.exec();
  
  while (query.next()) {
    projects.push_back(Project(query));
  }

  return projects;
}

QVector<User> SalaryDatabase::getConcreteProject(int id) {
  QVector<User> users;
  QSqlQuery query(db);
  query.prepare("SELECT users.id, users.username, users.password_hash, users.isDeleted, users.authority, users.fio, users.date_receipt, \
                users.date_dismissial, users.date_birth, users.isConfirmed, list_users.position, list_users.factor from list_users inner join users on \
                users.id = list_users.id_user where id_project=? and list_users.date_start<=CURDATE() group by users.fio, list_users.position");
  query.addBindValue(id);
  query.exec();

  while (query.next()) {
    User user(query);
    user.setPosition(query.value(10).toString());
    user.setMultiply(query.value(11).toDouble());
    users.push_back(user);
  }
  return users;
}

bool SalaryDatabase::updateProject(const Project & project) {
  QSqlQuery query(db);
  query.prepare("UPDATE project SET name=?, date_start=?, date_end=?, budget=?, count_dotation=? WHERE id=?");
  query.addBindValue(project.getProjectName());
  query.addBindValue(project.getDateStart());
  query.addBindValue(project.getDateEnd());
  query.addBindValue(project.getBudget());
  query.addBindValue(project.getCountDotation());
  query.addBindValue(project.getID());
  return query.exec();
}

bool SalaryDatabase::createProject(const Project & project) {
  QSqlQuery query("INSERT INTO project(name, date_start, date_end, budget, count_dotation) VALUES(?, ?, ?, ?, ?)");
  query.addBindValue(project.getProjectName());
  query.addBindValue(project.getDateStart());
  query.addBindValue(project.getDateEnd());
  query.addBindValue(project.getBudget());
  query.addBindValue(project.getCountDotation());
  return query.exec();
}

QVector<Position> SalaryDatabase::getAllPositions() {
  QSqlQuery query("SELECT * FROM list_position");
  query.exec();

  QVector<Position> positions;
  while (query.next()) {
    positions.push_back(Position(query));
  }

  return positions;
}

bool SalaryDatabase::addWorkerInProject(int id_worker, int id_project, const QString & position, double coef, const QString & project_end, const QString & worker_start) {
  QString req = "INSERT INTO list_users(id_user, id_project, position, factor, mark, date_start, date_end) VALUES(?, ?, ?, ?, NULL, \"" + worker_start + "\", \"#date_end#\")";
  QDate curdate = QDate::fromString(worker_start, "yyyy-MM-dd");
  QDate tmp = curdate;
  QDate project_end_date = QDate::fromString(project_end, QString("yyyy-MM-dd"));
  int cnt = 1;
  tmp.setDate(tmp.year(), tmp.month(), tmp.daysInMonth());
  if (tmp > project_end_date) {
    req.replace(QRegExp("#date_end#"), project_end);
  }
  else {
    req.replace(QRegExp("#date_end#"), tmp.toString("yyyy-MM-dd"));
    tmp = curdate;
    tmp.setDate(tmp.year(), tmp.month(), 1);
    tmp = tmp.addMonths(1);
    QString pattern(",(?, ?, ?, ?, NULL, \"#date_start#\", \"#date_end#\")");
    while (tmp.year() != project_end_date.year() || tmp.month() != project_end_date.month()) {
      QString query = pattern;
      tmp.setDate(tmp.year(), tmp.month(), 1);
      query.replace(QRegExp("#date_start#"), tmp.toString("yyyy-MM-dd"));
      tmp.setDate(tmp.year(), tmp.month(), tmp.daysInMonth());
      query.replace(QRegExp("#date_end#"), tmp.toString("yyyy-MM-dd"));
      tmp.setDate(tmp.year(), tmp.month(), 1);
      tmp = tmp.addMonths(1);
      req.append(query);
      ++cnt;
    }
    pattern.replace(QRegExp("#date_start#"), tmp.toString("yyyy-MM-dd"));
    pattern.replace(QRegExp("#date_end#"), project_end);
    req.append(pattern);
    ++cnt;
  }

  QSqlQuery query(req);
  while (cnt--) {
    query.addBindValue(id_worker);
    query.addBindValue(id_project);
    query.addBindValue(position);
    query.addBindValue(coef);
  }
  return query.exec();
}

bool SalaryDatabase::removeWorkerInProject(int id_worker, int id_project, const QString & position, const QString & worker_end, bool removeAllProject) {
  bool isOk;
  QSqlQuery query(db);
  if (removeAllProject) {
    query.prepare("DELETE FROM list_users WHERE id_user=? AND date_start>=?");
    query.addBindValue(id_worker);
    query.addBindValue(position);
    isOk = query.exec();
    query.prepare("UPDATE list_users SET date_end=? WHERE id_user=? AND date_start>=?");
    query.addBindValue(position);
    query.addBindValue(id_worker);
    QDate first_day_month = QDate::fromString(position, "yyyy-MM-dd");
    first_day_month.setDate(first_day_month.year(), first_day_month.month(), 1);
    query.addBindValue(first_day_month.toString("yyyy-MM-dd"));
  }
  else {
    query.prepare("DELETE FROM list_users WHERE id_user=? AND id_project=? AND position=? AND date_start>=?");
    query.addBindValue(id_worker);
    query.addBindValue(id_project);
    query.addBindValue(position);
    query.addBindValue(worker_end);
    isOk = query.exec();
    query.prepare("UPDATE list_users SET date_end=? WHERE id_user=? AND id_project=? AND position=? ORDER BY date_end DESC LIMIT 1");
    query.addBindValue(worker_end);
    query.addBindValue(id_worker);
    query.addBindValue(id_project);
    query.addBindValue(position);
  }

  return isOk && query.exec();
}

QVector<InfoForAccounting> SalaryDatabase::getForAccounting(int mounth, int year, bool searchOnlyNull) {
  QSqlQuery query; 
  if (searchOnlyNull) {
    query.prepare("SELECT users.id, users.fio, list_users.position, list_users.mark, project.id, project.name, list_users.date_start, list_users.date_end from list_users \
                  inner join users on users.id = list_users.id_user inner join project on project.id = list_users.id_project where list_users.date_start>=? and list_users.date_end<=? and list_users.mark is NULL");
  }
  else {
    query.prepare("SELECT users.id, users.fio, list_users.position, list_users.mark, project.id, project.name, list_users.date_start, list_users.date_end from list_users \
                  inner join users on users.id = list_users.id_user inner join project on project.id = list_users.id_project where list_users.date_start>=? and list_users.date_end<=?");
  }
  QDate date;
  date.setDate(year, mounth, 1);
  query.addBindValue(date.toString("yyyy-MM-dd"));
  date.setDate(year, mounth, date.daysInMonth());
  query.addBindValue(date.toString("yyyy-MM-dd"));
  query.exec();

  QVector<InfoForAccounting> result;
  while (query.next()) {
    InfoForAccounting tmp;
    tmp.id_user = query.value(0).toInt();
    tmp.fio = query.value(1).toString();
    tmp.position = query.value(2).toString();
    tmp.mark = query.value(3).toInt();
    tmp.id_project = query.value(4).toInt();
    tmp.project_name = query.value(5).toString();
    tmp.date_start = query.value(6).toString();
    tmp.date_end = query.value(7).toString();
    result.push_back(tmp);
  }
  return result;
}

bool SalaryDatabase::updateWorkerInAccounting(const InfoForAccounting & info) {
  QSqlQuery query("UPDATE list_users SET mark=? WHERE id_user=? AND id_project=? AND position=? AND date_start=? AND date_end=?");
  query.addBindValue(info.mark);
  query.addBindValue(info.id_user);
  query.addBindValue(info.id_project);
  query.addBindValue(info.position);
  query.addBindValue(info.date_start);
  query.addBindValue(info.date_end);
  return query.exec();
}

bool SalaryDatabase::removeUncorfimedWorker(int id) {
  QSqlQuery query("DELETE FROM users WHERE id=?");
  query.addBindValue(id);
  return query.exec();
}

bool SalaryDatabase::confirmedWorker(int id) {
  QSqlQuery query("UPDATE users SET isConfirmed=1 WHERE id=?");
  query.addBindValue(id);
  return query.exec();
}

ProjectWithDateWorkerForPayroll * SalaryDatabase::getFullInformation(int month, int year) {
  ProjectWithDateWorkerForPayroll * result = new ProjectWithDateWorkerForPayroll();
  QSqlQuery query(db);
  QDate date_begin_g;
  date_begin_g.setDate(year, month, 1);
  QDate date_end_g = date_begin_g;
  date_end_g.setDate(year, month, date_end_g.daysInMonth());

  query.prepare("SELECT * FROM list_users INNER JOIN project ON project.id=list_users.id_project WHERE list_users.date_start>=? AND list_users.date_end<=?");
  query.addBindValue(date_begin_g.toString("yyyy-MM-dd"));
  query.addBindValue(date_end_g.toString("yyyy-MM-dd"));
  query.exec();

  while (query.next()) {
    Project prj(query, 8);
    result->all_projects.push_back(prj);
    int id_other_worker = query.value(1).toInt();
    QString position = query.value(3).toString();
    int coef = query.value(4).toInt();
    int mark = query.value(5).toInt();
    if (!result->helpInfoOtherWorker[id_other_worker].contains(prj.getID())) {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] = 0;
    }
    QDate date_start = query.value(6).toDate();
    QDate date_end = query.value(7).toDate();
    QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
    if (project_date_end > date_end) {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] += date_start.daysTo(date_end) * result->convertCoef[mark] * coef;
    }
    else {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] += date_start.daysTo(project_date_end) * result->convertCoef[mark] * coef;
    }
  }

  QVector<Project> iterable_prj = result->all_projects;;
  for (auto id_other_worker : result->helpInfoOtherWorker.keys()) {
    for (auto prj : iterable_prj) {
      result->idToProject[prj.getID()] = prj;
      QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
      for (auto pos : result->helpInfoOtherWorker[id_other_worker][prj.getID()].keys()) {
        if (project_date_end > date_end_g) {
          result->helpInfoOtherWorker[id_other_worker][prj.getID()][pos] /= date_end_g.daysInMonth();
        }
        else {
          result->helpInfoOtherWorker[id_other_worker][prj.getID()][pos] /= date_begin_g.daysTo(project_date_end);
        }
      }
    }
  }

  return result;
}

ProjectWithDateWorkerForPayroll * SalaryDatabase::getProjectForWorkerOnDate(int id, int mounth, int year, bool ignore_id) {
  ProjectWithDateWorkerForPayroll * result = new ProjectWithDateWorkerForPayroll();
  QSqlQuery query(db);
  QDate date_begin_g;
  date_begin_g.setDate(year, mounth, 1);
  QDate date_end_g = date_begin_g;
  date_end_g.setDate(year, mounth, date_end_g.daysInMonth());
  if (ignore_id) {
    query.prepare("SELECT * FROM list_users INNER JOIN project ON project.id=list_users.id_project WHERE list_users.date_start>=? AND list_users.date_end<=?");
    query.addBindValue(date_begin_g.toString("yyyy-MM-dd"));
    query.addBindValue(date_end_g.toString("yyyy-MM-dd"));
    query.exec();
  }
  else {
    query.prepare("SELECT * FROM list_users INNER JOIN project ON project.id=list_users.id_project WHERE list_users.id_user=? AND list_users.date_start>=? AND list_users.date_end<=?");
    date_begin_g.setDate(year, mounth, 1);
    query.addBindValue(id);
    query.addBindValue(date_begin_g.toString("yyyy-MM-dd"));
    query.addBindValue(date_end_g.toString("yyyy-MM-dd"));
    query.exec();
    query.lastError().text();

    while (query.next()) {
      Project prj(query, 8);
      QString position = query.value(3).toString();
      int coef = query.value(4).toInt();
      int mark = query.value(5).toInt();
      if (!result->helpInfo.contains(prj.getID())) {
        result->projects.push_back(prj);
        result->helpInfo[prj.getID()][position] = 0;
      }
      QDate date_start = query.value(6).toDate();
      QDate date_end = query.value(7).toDate();
      QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
      if (project_date_end > date_end) {
        result->helpInfo[prj.getID()][position] += date_start.daysTo(date_end) * result->convertCoef[mark] * coef;
      }
      else {
        result->helpInfo[prj.getID()][position] += date_start.daysTo(project_date_end) * result->convertCoef[mark] * coef;
      }
    }

    for (auto prj : result->projects) {
      result->idToProject[prj.getID()] = prj;
      QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
      for (auto pos : result->helpInfo[prj.getID()].keys()) {
        if (project_date_end > date_end_g) {
          result->helpInfo[prj.getID()][pos] /= date_end_g.daysInMonth();
        }
        else {
          result->helpInfo[prj.getID()][pos] /= date_begin_g.daysTo(project_date_end);
        }
      }
    }

    // НИЖЕ ПОДГОТОВКА ДАННЫХ ДЛЯ ДРУГИХ РАБОТНИКОВ, А НЕ КОНКРЕТНОГО ID ИЗ ПАРАМЕТРА
    QString prepare1("SELECT * FROM list_users INNER JOIN project ON project.id=list_users.id_project WHERE (");
    QString prepare2(") AND list_users.id_user!=? AND list_users.date_start>=? AND list_users.date_end<=?");
    QString pattern("list_users.id_project=#id_project# OR ");
    QString insert_query;
    for (auto prj : result->projects) {
      insert_query.append(pattern);
      insert_query.replace(QRegExp("#id_project#"), QString::number(prj.getID()));
    }
    insert_query.remove(insert_query.size() - 4, 4);
    query.prepare(prepare1 + insert_query + prepare2);
    query.addBindValue(id);
    query.addBindValue(date_begin_g.toString("yyyy-MM-dd"));
    query.addBindValue(date_end_g.toString("yyyy-MM-dd"));
    query.exec();
  }
 
  while (query.next()) {
    Project prj(query, 8);
    result->all_projects.push_back(prj);
    int id_other_worker = query.value(1).toInt();
    QString position = query.value(3).toString();
    int coef = query.value(4).toInt();
    int mark = query.value(5).toInt();
    if (!result->helpInfoOtherWorker[id_other_worker].contains(prj.getID())) {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] = 0;
    }
    QDate date_start = query.value(6).toDate();
    QDate date_end = query.value(7).toDate();
    QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
    if (project_date_end > date_end) {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] += date_start.daysTo(date_end) * result->convertCoef[mark] * coef;
    }
    else {
      result->helpInfoOtherWorker[id_other_worker][prj.getID()][position] += date_start.daysTo(project_date_end) * result->convertCoef[mark] * coef;
    }
  }

  QVector<Project> iterable_prj = result->projects;
  if (ignore_id) {
    iterable_prj = result->all_projects;
  }
  for (auto id_other_worker : result->helpInfoOtherWorker.keys()) {
    for (auto prj : iterable_prj) {
      result->idToProject[prj.getID()] = prj;
      QDate project_date_end = QDate::fromString(prj.getDateEnd(), QString("yyyy-MM-dd"));
      for (auto pos : result->helpInfoOtherWorker[id_other_worker][prj.getID()].keys()) {
        if (project_date_end > date_end_g) {
          result->helpInfoOtherWorker[id_other_worker][prj.getID()][pos] /= date_end_g.daysInMonth();
        }
        else {
          result->helpInfoOtherWorker[id_other_worker][prj.getID()][pos] /= date_begin_g.daysTo(project_date_end);
        }
      }
    }
  }

  return result;
}

QVector<Prikaz> SalaryDatabase::getAllPrikazes() {
  QVector<Prikaz> prikazes;
  QSqlQuery query(db);
  query.prepare("SELECT prikaz.id, prikaz.id_creator, prikaz.id_user, users.FIO, prikaz.type_prikaz, \
                prikaz.date_create FROM prikaz INNER JOIN users ON prikaz.id_user = users.id ");
  query.exec();

  while (query.next()) {
    prikazes.push_back(Prikaz(query));
  }

  return prikazes;
}

bool SalaryDatabase::createPrikaz(bool typeOfPrikaz, int idUser, int idCreator, QString date) {
  QSqlQuery query("INSERT INTO prikaz(type_prikaz, id_user, id_creator, date_create) VALUES(?, ?, ?, ?)");
  query.addBindValue(typeOfPrikaz);
  query.addBindValue(idUser);
  query.addBindValue(idCreator);
  query.addBindValue(date);
  return query.exec();
}

bool SalaryDatabase::updatePrikaz(bool typeOfPrikaz, int idUser, QString date, QString previousDate) {
  QSqlQuery query(db);
  query.prepare("UPDATE prikaz SET date_create=? WHERE id_user=? AND type_prikaz = ? AND date_create = ?");
  query.addBindValue(date);
  query.addBindValue(idUser);
  query.addBindValue(typeOfPrikaz);
  query.addBindValue(previousDate);
  return query.exec();
}

bool SalaryDatabase::workerExistInProject(int id_project) {
  QSqlQuery query("SELECT id FROM list_users WHERE id_project=? GROUP BY date_start ASC LIMIT 1");
  query.addBindValue(id_project);
  query.exec();
  return query.next();
}

bool SalaryDatabase::addNewPosition(QString position) {
  QSqlQuery query("INSERT INTO list_position(position) VALUES(?)");
  query.addBindValue(position);
  return query.exec();
}

bool SalaryDatabase::deletePosition(QVariantList idPosition) {
  QSqlQuery query("DELETE FROM list_position WHERE id=?");
  query.addBindValue(idPosition);
  return query.execBatch();
}

bool SalaryDatabase::editPosition(int idPosition, QString position) {
  QSqlQuery query("UPDATE list_position SET position=? WHERE id=?");
  query.addBindValue(position);
  query.addBindValue(idPosition);
  return query.exec();
}

void SalaryDatabase::checkDatabase() {
  QSqlQuery query("SHOW TABLES");
  query.exec();

  QSet<QString> tables;
  tables.insert("list_position");
  tables.insert("list_users");
  tables.insert("prikaz");
  tables.insert("project");
  tables.insert("users");

  while (query.next()) {
    tables.remove(query.value(0).toString());
  }

  for (auto it : tables) {
    if (it == "list_position") {
      query.prepare("CREATE TABLE `list_position` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `position` text NOT NULL,\
                    PRIMARY KEY(`id`)\
                    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8");
      query.exec();
    }
    else if (it == "list_users") {
      query.prepare("CREATE TABLE `list_users` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `id_user` int(11) NOT NULL,\
                    `id_project` int(11) NOT NULL,\
                    `position` text NOT NULL,\
                    `factor` float NOT NULL,\
                    `mark` int(11) DEFAULT NULL,\
                    `date_start` date NOT NULL,\
                    `date_end` date NOT NULL,\
                    PRIMARY KEY(`id`)\
                    ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8");
      query.exec();
    }
    else if (it == "prikaz") {
      query.prepare("CREATE TABLE `prikaz` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `type_prikaz` tinyint(1) NOT NULL,\
                    `id_user` int(11) NOT NULL,\
                    `id_creator` int(11) NOT NULL,\
                    `date_create` date NOT NULL,\
                    PRIMARY KEY(`id`)\
                    ) ENGINE = InnoDB AUTO_INCREMENT = 64 DEFAULT CHARSET = utf8");
      query.exec();
    }
    else if (it == "project") {
      query.prepare("CREATE TABLE `project` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `name` text NOT NULL,\
                    `date_start` date NOT NULL,\
                    `date_end` date NOT NULL,\
                    `budget` int(11) NOT NULL,\
                    `count_dotation` int(11) NOT NULL,\
                    PRIMARY KEY(`id`)\
                    ) ENGINE = InnoDB AUTO_INCREMENT = 40 DEFAULT CHARSET = utf8; ");
      query.exec();
    }
    else if (it == "users") {
      query.prepare("CREATE TABLE `users` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `username` text NOT NULL,\
                    `password_hash` text NOT NULL,\
                    `isDeleted` int(11) NOT NULL,\
                    `authority` int(11) NOT NULL,\
                    `fio` text NOT NULL,\
                    `date_receipt` date NOT NULL,\
                    `date_dismissial` date DEFAULT NULL,\
                    `date_birth` date DEFAULT NULL,\
                    `isConfirmed` tinyint(1) NOT NULL,\
                    `passport_number` text DEFAULT NULL,\
                    `passport_code` text DEFAULT NULL,\
                    `passport_date` text DEFAULT NULL,\
                    `passport_tag` text DEFAULT NULL,\
                    PRIMARY KEY(`id`)\
                    ) ENGINE = InnoDB AUTO_INCREMENT = 204 DEFAULT CHARSET = utf8");
      query.exec();
    }
  }
}