﻿#include "position.h"

Position::Position()
{
}


Position::~Position()
{
}

Position::Position(const QSqlQuery & query) {
  this->id = query.value(0).toInt();
  this->position = query.value(1).toString();
}

int Position::getId() const {
  return this->id;
}

QString Position::getPosition() const {
  return this->position;
}

void Position::setPosition(QString position) {
  this->position = position;
}

void Position::setId(int id) {
  this->id = id;
}