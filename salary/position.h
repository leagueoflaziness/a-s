﻿#pragma once
#include <QSqlQuery>
#include <QVariant>

class Position
{
public:
  Position();
  explicit Position(const QSqlQuery & query);
  ~Position();

  int getId() const;
  QString getPosition() const;
  void setPosition(QString position);
  void setId(int id);

private:
  int id;
  QString position;
};

